# GameOfLife

## Authors

- José Hisse
- Marcos Eduardo

## Getting started

Ao iniciar o jogo, será necessário colocar o tamanho da janela desejado.

- Digite o tamanho da janela LarguraxAltura
- Coloque o tamanho com separação de um 'x', exemplo: 500x500, 900x900

## Description

O jogo da vida é um autómato celular desenvolvido pelo matemático britânico John Horton Conway em 1970. É o exemplo mais
bem conhecido de autômato celular

## Installation

```pip install pygame```

## Run

```python life.py```

