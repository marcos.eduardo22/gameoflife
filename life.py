import random
import time

import pygame  # pip install pygame


class GameOfLife:
    # Inicializa variáveis necessárias ao jogo
    def __init__(self, largura, altura):
        self.largura = int(largura)
        self.altura = int(altura)

        self.grid_largura = int(self.largura / 10)
        self.grid_altura = int(self.altura / 10)

        self.populacao = 0

        # Inicia um plano vazio
        self.plano = [[0 for _ in range(self.grid_largura)]
                      for _ in range(self.grid_altura)]

        pygame.init()
        self.screen = pygame.display.set_mode((self.largura, self.altura + 50))
        pygame.display.set_caption('Game of Life')

    def desenha(self):
        # Preenche a tela com fundo azul
        self.screen.fill([0, 0, 128])

        # Desenha grid
        for row in range(0, self.largura, 10):
            for cell in range(0, self.altura, 10):
                pygame.draw.line(self.screen, (20, 20, 128),
                                 (cell, row), (cell, self.largura))
                pygame.draw.line(self.screen, (20, 20, 128),
                                 (cell, row), (self.altura, row))

        # Percorre o plano desenhando as celulas vivas
        for r, row in enumerate(self.plano):
            for c, cell in enumerate(row):
                if cell:
                    pygame.draw.rect(
                        self.screen, (255, 255, 255), (10 * c, 10 * r, 9, 9))

        # barra inferior preta
        pygame.draw.rect(self.screen, (0, 0, 0), pygame.Rect(
            0, self.altura, self.largura, 50))

    def processa(self):
        # Cria um plano temporário
        plano_temporario = [
            [0 for _ in range(self.grid_largura)] for _ in range(self.grid_altura)]

        # Processa o plano temporário
        for x in range(self.grid_altura):
            for y in range(self.grid_largura):
                vivos = self.vizinhos_vivos(x, y, self.plano)

                # Regra 1 - Qualquer célula viva com menos de dois vizinhos vivos morre.
                if self.plano[x][y] == 1 and vivos < 2:
                    plano_temporario[x][y] = 0
                    self.populacao -= 1

                # Regra 2 - Qualquer célula viva com dois ou três vizinhos vivos continua viva para a próxima geração.
                if self.plano[x][y] == 1 and (vivos == 2 or vivos == 3):
                    plano_temporario[x][y] = 1

                # Regra 3 - Qualquer célula viva com mais de três vizinhos vivos morre.
                if self.plano[x][y] == 1 and vivos > 3:
                    plano_temporario[x][y] = 0
                    self.populacao -= 1

                # Regra 4 - Qualquer célula morta com três vizinhos vivos volta a vida.
                if self.plano[x][y] == 0 and vivos == 3:
                    plano_temporario[x][y] = 1
                    self.populacao += 1

        # Define plano atual como o plano temporário
        self.plano = [[value for value in row] for row in plano_temporario]

    def vizinhos_vivos(self, x, y, plano):
        # Retorna a quantidade de vizinhos vivos da célula (x,y)
        vivos = (
            plano[(x - 1) % self.grid_altura][(y - 1) % self.grid_largura] +
            plano[(x - 1) % self.grid_altura][y] +
            plano[(x - 1) % self.grid_altura][(y + 1) % self.grid_largura] +
            plano[x][(y - 1) % self.grid_largura] +
            plano[x][(y + 1) % self.grid_largura] +
            plano[(x + 1) % self.grid_altura][(y - 1) % self.grid_largura] +
            plano[(x + 1) % self.grid_altura][y] +
            plano[(x + 1) % self.grid_altura][(y + 1) % self.grid_largura]
        )
        return vivos

    def button(self, position, text):
        font = pygame.font.SysFont("Arial", 42)
        text_render = font.render(text, True, (255, 0, 0))
        _, _, w, h = text_render.get_rect()
        x, y = position
        size = font.size(text)
        x = self.largura / 2 - size[0] / 2
        pygame.draw.line(self.screen, (150, 150, 150), (x, y), (x + w, y), 5)
        pygame.draw.line(self.screen, (150, 150, 150),
                         (x, y - 2), (x, y + h), 5)
        pygame.draw.line(self.screen, (50, 50, 50),
                         (x, y + h), (x + w, y + h), 5)
        pygame.draw.line(self.screen, (50, 50, 50),
                         (x + w, y + h), [x + w, y], 5)
        pygame.draw.rect(self.screen, (100, 100, 100), (x, y, w, h))
        return self.screen.blit(text_render, (x, y))

    def mensagem(self, position, text):
        font = pygame.font.SysFont("Arial", 30)
        text_render = font.render(text, True, (255, 255, 255))
        x, y = position

        return self.screen.blit(text_render, (x, y))

    def game_of_life(self):
        run = True
        speed = 0.1
        text_speed = '1x'
        while run:
            # processa a nova geração
            self.processa()

            # desenha a nova geração na tela
            self.desenha()
            # texto/butao de velocidade
            velocidade = self.mensagem(
                (self.largura - 150, self.altura), f"Speed: {text_speed}")
            # Quantidade populacional
            self.mensagem((0, self.altura), f"População: {self.populacao}")

            pygame.display.update()

            # Verificação de evento
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # sai do programa clicando no X da janela
                    run = False
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    # sai do programa apertando ESC
                    run = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if velocidade.collidepoint(pygame.mouse.get_pos()):
                        speed /= 2
                        text_speed = '2x'
                        if speed <= 0.025:
                            speed = 0.1
                            text_speed = '1x'

            # esperar antes de ir para proxima geração
            time.sleep(speed)

    def conta_populacao(self):
        # Conta a população inicial do game
        self.populacao = sum(sum(self.plano, []))

    def aleatorio(self):
        self.plano = [[random.choice([0, 1]) for _ in range(self.grid_altura)]
                      for _ in range(self.grid_largura)]
        # desenha a geração inicial
        self.desenha()
        pygame.display.update()
        # espera alguns segundos para podermos ver a geração inicial antes de começar
        time.sleep(1.5)
        self.conta_populacao()
        self.game_of_life()

    def escolher(self):
        self.desenha()
        # loop para o usuario desenhar como quer o joho
        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # sai do programa clicando no X da janela
                    run = False
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    # sai do programa apertando ESC
                    run = False
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    self.conta_populacao()
                    return self.game_of_life()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x, y = pygame.mouse.get_pos()
                    if y <= self.altura:
                        if self.plano[int(y / 10)][int(x / 10)]:
                            self.plano[int(y / 10)][int(x / 10)] = 0
                        else:
                            self.plano[int(y / 10)][int(x / 10)] = 1
                        self.desenha()

            self.mensagem((0, self.altura), "ENTER para iniciar")
            pygame.display.update()

    def menu(self):
        b1 = self.button(((self.largura / 3), (self.altura / 3)), "Escolher")
        b2 = self.button(
            ((self.largura / 3), (self.altura / 3) + 100), "Aleatorio")
        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        run = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if b1.collidepoint(pygame.mouse.get_pos()):
                        return self.escolher()
                    elif b2.collidepoint(pygame.mouse.get_pos()):
                        return self.aleatorio()
            pygame.display.update()

        pygame.quit()


if __name__ == "__main__":
    width, height = input(
        "Digite o tamanho da janela LxA (Recomendado: 500x500): ").split('x')
    GameOfLife(width, height).menu()
